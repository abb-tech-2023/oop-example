import Task4.ConsoleLogger;
import Task4.FileLogger;
import Task4.Logger;
import Task5.RentalTransaction;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
      /*  EconomyCar economyCar = new EconomyCar("Tayota","Corolla", 2021, 50, 10);
        economyCar.displayCarInfo();
        double economyCarRentalCharge = economyCar.calculateRentalCharge(7);
        System.out.println("Rental Charge: $" + economyCarRentalCharge);
        LuxuryCar luxuryCar = new LuxuryCar("Mercedes-Benz", "S-Class", 2022, 50, "Leather Seats" );
        luxuryCar.displayCarInfo();
        double luxuryCarRentalCharge = luxuryCar.calculateRentalCharge(3);
        System.out.println("Rental Charge: $" + luxuryCarRentalCharge);
    }
}*/
       /* Logger consoleLogger = new ConsoleLogger();
        consoleLogger.logInfo("This is an information message.");
        consoleLogger.logWarning("This is a warning message.");
        consoleLogger.logError("This is an error message.");

        Logger fileLogger = new FileLogger();
        fileLogger.logInfo("This is an information message.");
        fileLogger.logWarning("This is a warning message.");
        fileLogger.logError("This is an error message.");
    }
}*/  Car car= new Car("KIA", "Sorento", 2013, 0.7) {
            @Override
            public boolean rent(int numDays) {
                return false;
            }

            @Override
            public boolean returnCar() {
                return false;
            }

            @Override
            public double calculateRentalCharge(int numDays) {
                return 0;
            }
        };

        RentalTransaction rental = new RentalTransaction();
        rental.setCar(car);
        rental.setCustomerName("Ali");
        rental.setRentalDays(LocalDate.of(2023, 7, 1));
        rental.displayTransactionInfo();

    }
}




