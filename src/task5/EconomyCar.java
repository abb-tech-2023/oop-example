package src.task5;

public class EconomyCar extends Car {
    private double fuelEfficiency;

    public EconomyCar(String make, String model, int year, double rentalRate, double fuelEfficiency) {
        super(make, model, year, rentalRate);
        this.fuelEfficiency = fuelEfficiency;
    }

    public double getFuelEfficiency() {
        return fuelEfficiency;
    }

    public void setFuelEfficiency(double fuelEfficiency) {
        this.fuelEfficiency = fuelEfficiency;
    }

    @Override
    public double calculateRentalCharge(int numDays) {
        double charge = 5.55;
        return charge;
    }

    @Override
    public boolean rent(int numDays) {
        boolean available;
        if (numDays < 5) {
            available = false;
        } else {
            available = true;
        }
        return available;
    }

    @Override
    public boolean returnCar() {
        return false;
    }
}


