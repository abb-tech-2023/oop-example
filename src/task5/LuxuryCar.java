package src.task5;

public class LuxuryCar extends Car {
    private String[] premiumFeatures;

    public LuxuryCar(String make, String model, int year, double rentalRate, String premiumFeatures) {
        super(make, model, year, rentalRate);
        this.premiumFeatures = new String[]{premiumFeatures};
    }

    public String[] getPremiumFeatures() {
        return premiumFeatures;
    }

    public void setPremiumFeatures(String[] premiumFeatures) {
        this.premiumFeatures = premiumFeatures;
    }

    @Override
    public double calculateRentalCharge(int numDays) {
        double charge = numDays*11.11;
        return charge;
    }

    @Override
    public boolean rent(int numDays) {
        boolean available;
        if( numDays > 10){
            available = true;
        }else {
            available=false;
        }
        return available;
    }

    @Override
    public boolean returnCar() {
        return false;
    }
}



