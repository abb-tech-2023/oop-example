package src.task5;

public interface Rentable {
    boolean rent(int numDays);
    boolean returnCar();
}

