package src.task5;


import java.time.LocalDate;

public class RentalTransaction {
    private Car car;
    private String customerName;
    private int rentalDays;
    private LocalDate RentalDays;

    private Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getRentalDays() {
        return rentalDays;
    }

    public void setRentalDays(LocalDate rentalDays) {
        this.RentalDays = rentalDays;
    }

    public double calculateRentalCost() {
        double totalCost = car.getRentalRate() * rentalDays;
        return totalCost;
    }

    public void displayTransactionInfo() {
        System.out.println("Transaction Information:");
        System.out.println("src.task5.Car Details: " + car.toString());
        System.out.println("Customer Name: " + customerName);
        System.out.println("Rental Days: " + rentalDays);
        System.out.println("Rental Cost: $" + calculateRentalCost());
    }
}



